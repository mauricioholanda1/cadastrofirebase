import {createAppContainer, createStackNavigator} from 'react-navigation';
import Login from '../pages/Login';
import Main from '../pages/Main';
// import the different screens
import SignUp from '../pages/SignUp';

// create our app's navigation stack
const Routes = createStackNavigator(
  {
    Login,
    SignUp,
    Main,
  },
  {
    initialRouteName: 'Login',
  },
);

export default createAppContainer(Routes);
